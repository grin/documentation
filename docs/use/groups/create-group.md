# Create and edit a group

 Groups are spaces for coordination and preparation. They can be used to improve events organization and to manage your community.

## Creation

=== "How to"
     To create one, you have to:

      1. click **My groups** button on top bar menu
      * click **Create group** button
      * (required) enter your group name in **Group display name** field
      * (required) enter the federated groupe name you want (or just keep the one generated with your group display name) in **Federated Group Name** field (this is like your federated username for groups. It will allow you to be found on the federation, and is guaranteed to be unique)
      * enter a description of what your group is about in **Description** field
      * add an avatar for your group by selecting an image on your device, by clicking **Click to upload** button
      * add a banner for your group by selecting an image on your device, by clicking **Click to upload** button
      * click **Create my group** button

=== "Screenshot"
    ![creation group img](../../images/en/group-creation-EN.png)

## Settings

=== "How to"
    You can access to your group settings by:

      1. clicking **My groups** button on top bar menu
      * clicking the one you want to invite people on the list
      * clicking **Group settings** button in your group banner

    In this section you can edit informations added at the group creation (see above), like group name, description, avatar and banner. You also can:

      * change **Group visibility**:
        * **Visible everywhere on the web**: the group will be publicly listed in search results and may be suggested in the explore section. Only public informations will be shown on its page.
        * **Only accessible through link**: you'll need to transmit the group URL so people may access the group's profile. The group won't be findable in Mobilizon's search or regular search engines.
      * allow to join (or not) a group:
        * **Anyone can join freely**: anyone wanting to be a member from your group will be able to from your group page
        * **Manually invite new members**: the only way for your group to get new members is if an admininistrator invites them
      * add **Group address**

=== "Screenshot"
    ![creation settings img](../../images/en/group-creation-settings-EN.png)
