# Docker

You can easily run the `framasoft/mobilizon` docker image standalone, but since Mobilizon requires PostgreSQL and nginx, we recommend using docker-compose.

The docker-compose provides an app container for Mobilizon itself and a database container for PostgreSQL (Postgis). You'll need your own reverse-proxy to handle TLS termination (on port 4000).

## Get the docker-compose repo

```bash
git clone https://framagit.org/framasoft/joinmobilizon/docker.git docker-mobilizon
cd docker-mobilizon
```

## Update the env file

```bash
cp env.template .env
```

Edit the `.env` content with your own settings.
More settings can be added in .env, see all in docker-compose.yml file.

You can generate values for `MOBILIZON_INSTANCE_SECRET_KEY_BASE` and `MOBILIZON_INSTANCE_SECRET_KEY` with:

```bash
gpg --gen-random --armor 1 50
```

## Advanced configuration

You can setup advanced configuration through the `config.exs` Elixir file. To get a file, `docker cp mobilizon:/etc/mobilizon/config.exs .`

It defaults to using the environment variables from `.env` but you may edit the existing parts and add any configuration setting you want.

To activate using the `config.exs` file, you may uncomment the corresponding volume line into `docker-compose.yml`, then simply restart the `mobilizon` container.

## Run the service

Start both the database and the Mobilizon service. The Mobilizon service will wait for the database one.

```bash
docker-compose up -d
```

The eventual migrations will be automatically run before starting Mobilizon.

## Run a mobilizon_ctl command

Any task documented in [CLI tasks](../CLI tasks/index.md) can be executed on Docker as well, but it need to be adapted, as the `mix` tool is not available in releases which are used in Docker images. Instead, we provide the `mobilizon_ctl` wrapper tool that can call any Mobilizon `mix` task (namedspaced under `mobilizon.`).

```bash
docker-compose exec mobilizon mobilizon_ctl [options]
```

For instance, creating a new admin without Docker would be something like:
```bash
MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

Inside Docker, it would be something like:
```bash
docker-compose exec mobilizon mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

## Update the service

Pull the latest image, then update the service. The migrations are automatically performed:

```bash
docker-compose pull framasoft/mobilizon
docker-compose up -d
```
